const path = require('path');
const BabiliPlugin = require('babili-webpack-plugin');

module.exports = {
  devtool: 'source-map',
  entry: {
    app: './src/app.js',
    tests: './src/tests.js'
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'dist')
  },
  plugins: [
    new BabiliPlugin()
  ]
};
