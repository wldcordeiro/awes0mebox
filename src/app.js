import { render } from './rekt';
import { createStore } from './utils';
import { fetchImages } from './api';
import { App } from './components';
import * as C from './constants';

const initState = {
  imageData: { status: 'pending', images: [] },
  selectedImg: null
};

/*
 * appReducer - The state reducer for the application.
 *
 * This handles four actions.
 * * TOGGLE_LIGHTBOX - loads a selected image into the lightbox or closes it
 * * NEXT_IMAGE - changes the selected image in the lightbox to the next image
 *                in the grid.
 *   PREV_IMAGE - same as NEXT_IMAGE only it goes in the reverse order.
 *   FETCH_IMAGES - used for retrieving the images from Imgur's API, it has
 *                  three possible states, pending, success and error.
 *                  Updates our `imageData` state object with the status of our
 *                  fetch and the images returned.
 */
function appReducer(state = initState, action) {
  switch (action.type) {
    case C.TOGGLE_LIGHTBOX:
      state.selectedImg = action.selectedImg;
      return state;
    case C.NEXT_IMAGE:
      if (state.selectedImg == state.imageData.images.length - 1) {
        state.selectedImg = 0;
      } else {
        state.selectedImg = state.selectedImg + 1;
      }
      return state;
    case C.PREV_IMAGE:
      if (state.selectedImg == 0) {
        state.selectedImg = state.imageData.images.length - 1;
      } else {
        state.selectedImg = state.selectedImg - 1;
      }
      return state;
    case C.FETCH_IMAGES:
      if (action.status == 'success') {
        state.imageData.status = action.status;
        state.imageData.images = action.images;
        return state;
      }

      if (action.status == 'error') {
        state.imageData.status = action.status;
        state.imageData.images = action.images;
        return state;
      }

      return state;
    default:
      return state;
  }
}

const { getState, dispatch, subscribe } = createStore(appReducer);

/*
 * An object containing our event handlers for the application.
 * These dispatch their appropriate actions.
 */
const handlers = {
  handleImageClick(selectedImg) {
    dispatch({ type: C.TOGGLE_LIGHTBOX, selectedImg });
  },
  handleLightboxClose(e) {
    dispatch({ type: C.TOGGLE_LIGHTBOX, selectedImg: null });
  },
  handleLightboxPrev() {
    dispatch({ type: C.PREV_IMAGE });
  },
  handleLightboxNext() {
    dispatch({ type: C.NEXT_IMAGE });
  },
};

/*
 * runApp is our main entry for the application. It gets our state from the
 * redux like store and renders the application to our mount point.
 */
function runApp() {
  const { imageData, selectedImg } = getState();
  const mount = document.querySelector('#mount');
  render(App({ imageData, selectedImg, handlers }), mount);
}

// Subscribe our runApp function to the dispatcher.
subscribe(runApp);
// Initial application paint.
runApp();
// Initialize our image fetch with our thunk.
fetchImages()(dispatch);
