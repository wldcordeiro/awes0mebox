/*
 * createStore - Creates basic redux-like store system
 *
 * Exports three functions, getState, dispatch and subscribe
 *
 * getState - returns the state object.
 * dispatch - sends actions through  the reducer and calls any listeners.
 * subscribe - registers a listener.
 */
export function createStore(reducer) {
  let state;
  let listeners = [];

  function getState() {
    return state;
  }

  function dispatch(action) {
    state = reducer(state, action);
    listeners.forEach(listener => listener());
  }

  function subscribe(listener) {
    listeners.push(listener);

    return function() {
      listeners = listeners.filter(l => l !== listener);
    }
  }

  dispatch({});

  return { getState, dispatch, subscribe };
}


/*
 * fetch - A function that returns an XMLHttpRequest wrapped in a Promise
 *
 * It takes a URL and makes a GET request to that URL, adds the authorization
 * header needed for Imgur.
 */
export function fetch(url) {
  return new Promise((resolve, reject) => {
    const request = new XMLHttpRequest();
    request.open('GET', url);
    request.setRequestHeader('Authorization', 'Client-ID a47c41ffaa4c2ea');

    request.onload = () => {
      if (request.status == 200) {
        resolve(request.response);
      } else {
        reject(Error(request.statusText));
      }
    };

    request.onerror = () => {
      reject(Error('Network Error!'));
    };

    request.send();
  });
}
