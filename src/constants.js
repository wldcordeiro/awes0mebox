// Action constants.
export const FETCH_IMAGES = 'FETCH_IMAGES';
export const TOGGLE_LIGHTBOX = 'TOGGLE_LIGHTBOX';
export const NEXT_IMAGE = 'NEXT_IMAGE';
export const PREV_IMAGE = 'PREV_IMAGE';
