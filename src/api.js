import { fetch } from './utils';
import { FETCH_IMAGES } from './constants';

/*
 * fetchImages - a thunk for fetching the images from imgur.
 *
 * It returns a function that dispatches the relevant actions,
 * makes the API calls for the first five pages of Imgur, joins all the results
 * together, and extracts the data into a format our components expect.
 */
export function fetchImages() {
  return function(dispatch) {
    dispatch({ type: FETCH_IMAGES });

    const pagePromises = Array.from({ length: 5},
      (_, i) => fetch(`https://api.imgur.com/3/gallery/hot/viral/${i}`)
      .then(response => JSON.parse(response).data));

    Promise.all(pagePromises)
      .then(pages => pages.reduce((acc, page) => acc.concat(page)), [])
      .then(apiImages => {
        const formattedImgs = apiImages
          .filter(image => !image.is_album &&
            !image.link.endsWith('gif') && image.nsfw != true)
          .map((image, index) => ({
            src: image.link,
            index: index,
            caption: image.title,
          }));

        dispatch({
          type: FETCH_IMAGES,
          status: 'success',
          images: formattedImgs
        });
      })
      .catch(err => {
        console.log(err);
        dispatch({ type: FETCH_IMAGES, status: 'error', images: [] });
      });
  }
}

