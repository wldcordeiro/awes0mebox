import { createElement as elem } from './rekt';

// SVGs

// Components

/*
 * ImageCard - the image card used in the grid and lightbox
 *
 * takes the image source, index, caption, clickhandler and a booliean
 * to indicate if it's being used in the lightbox or grid.
 */
function ImageCard({ src, index, caption, handleClick, isLightbox }) {
  return elem('figure.img-card', {
    onClick() {
      if (!isLightbox) {
        handleClick(index);
      }
    }
  }, [
    elem('img', { src }),
    elem('div.caption', {}, caption)
  ]);
}

/*
 * Lightbox - the lightbox for displaying a fullsize image.
 *
 * takes the image object containing the source, index and caption, along
 * with handlers for navigating to the next/previous images and closing the
 * lightbox.
 */
function Lightbox({ image, handlePrev, handleNext, handleClose }) {
  const lightbox = elem('div.lightbox-container', {}, [
    elem('button.lightbox-nav.close', { onClick: handleClose }, '⨯'),
    elem('button.lightbox-nav.back', { onClick: handlePrev }, '◀'),
    ImageCard({
      src: image.src,
      index: image.index,
      caption: image.caption,
      isLightbox: true,
    }),
    elem('button.lightbox-nav.forward', { onClick: handleNext }, '▶'),
  ]);

  return lightbox;
}

/*
 * App - the main application container component.
 *
 * takes our imageData (the status of our fetch and array of images), the
 * selected image index and an object containing the handlers for our other
 * components.
 *
 * It checks the status of our image fetch and renders a loading indicator,
 * the image grid or a message for when there's an error.
 *
 * If we have a selected image it renders the lightbox.
 */
export function App({ imageData, selectedImg, handlers }) {
  const {
    handleImageClick,
    handleLightboxPrev,
    handleLightboxNext,
    handleLightboxClose,
  } = handlers;
  let lightbox;
  if (selectedImg != null) {
    lightbox = Lightbox({
      image: imageData.images[selectedImg],
      handlePrev: handleLightboxPrev,
      handleNext: handleLightboxNext,
      handleClose: handleLightboxClose
    });
  }

  let imageElems;

  if (imageData.status == 'error') {
    imageElems = [elem('h1.empty-msg', {},
      "There doesn't seem to be any images :(")];
  } else if (imageData.status == 'success') {
    imageElems = imageData.images
      .map((image, index) => ImageCard({
        src: image.src,
        index: image.index,
        caption: image.caption,
        isLightbox: false,
        handleClick:  handleImageClick
      }));
  } else {
    imageElems = [
      elem('img.loading-images', { src: '../img/loading.gif' })
    ];
  }

  return elem('div.wrapper', {}, [
    elem('main', {}, [
      elem('header', {}, [
        elem('h1', {}, 'Awes0mebox!'),
      ]),
      elem('section.img-grid', {}, imageElems),
    ]),
    lightbox
  ]);
}
