import { start, test, assert } from 'qunitjs';
import { createElement as elem, render } from './rekt';
import { createStore } from './utils';

start();

test('Rekt - elem - basic', () => {
  const testElem = elem('p', {}, 'test content');
  assert.equal(testElem.type, 'p', 'Created paragraph tag');
  assert.equal(testElem.props.children, 'test content', 'Created text content');
});

test('Rekt - elem - nested', () => {
  const testElem = elem('div', {}, [
    elem('p', {}, 'Heyo!')
  ]);
  assert.equal(testElem.type, 'div', 'Created div tag');
  assert.equal(testElem.props.children[0].type, 'p', 'Created nested p tag');
  assert.equal(
    testElem.props.children[0].props.children,
    'Heyo!', 'Created text content in nested p tag'
  );
});

test('Rekt - elem - w/ class', () => {
  const testElem = elem('p.special', {}, 'test content');
  assert.equal(testElem.type, 'p', 'Created paragraph tag');
  assert.equal(testElem.props.children, 'test content', 'Created text content');
  assert.deepEqual(testElem.props.className, 'special', 'Created class');
});

test('Rekt - elem - w/ multiple classes', () => {
  const testElem = elem('p.special.para', {}, 'test content');
  assert.equal(testElem.type, 'p', 'Created paragraph tag');
  assert.equal(testElem.props.children, 'test content', 'Created text content');
  assert.deepEqual(testElem.props.className, 'special para', 'Created classes');
});

test('Rekt - render', () => {
  const testElem = elem('p.special', {
    className: 'test'
  }, 'test content');
  const mount = document.querySelector('#render-test-mount');
  render(testElem, mount);
  const elemNode = document.querySelector('#render-test-mount p.special');

  assert.equal(elemNode.parentNode, mount, 'element is mounted as child');
  assert.equal(elemNode.tagName, 'P', 'element is of the right tag');
  assert.equal(elemNode.textContent,
    'test content', 'element text content matches VDOM');
  assert.equal(elemNode.className,
    'special test', 'element class is set properly');
});

test('Rekt - render - nested', () => {
  const testElem = elem('div', {}, [
    elem('p', {}, 'Heyo!')
  ]);
  const mount = document.querySelector('#render-test-mount');
  render(testElem, mount);
  const elemNode = document.querySelector('#render-test-mount div');
  assert.equal(elemNode.parentNode, mount, 'element is mounted as child');
  assert.equal(elemNode.tagName, 'DIV', 'element is of the right tag');
  assert.equal(elemNode.children[0].parentNode,
    elemNode, 'element child has the right parent');
  assert.equal(elemNode.children[0].tagName,
    'P', 'element child is of the right tag');
});

test('Utils - createStore', () => {
  function testReducer(state = 0, action) {
    switch (action.type) {
      case 'INCREMENT':
        return state + 1;
      case 'DECREMENT':
        return state - 1;
      default:
        return state;
    }
  }

  const { getState, dispatch, subscribe } = createStore(testReducer);
  let testState;
  function start() {
    testState = getState();
  }
  subscribe(start);
  start();

  assert.equal(testState, 0, 'State starts at initial value of 0');
  dispatch({ type: 'INCREMENT' });
  dispatch({ type: 'INCREMENT' });
  assert.equal(testState, 2, 'State increments');
  dispatch({ type: 'DECREMENT' });
  assert.equal(testState, 1, 'State decrements');
});
