# Awes0mebox! - An image viewer and lightbox

Awes0mebox is a simple image viewer application designed for the Slack technical challenge. It implements a very simple React+Redux like architecture with a toy library dubbed `Rekt`.

You can view Awes0mebox in production at http://awes0mebox.surge.sh

## Building Awes0mebox

To build Awes0mebox you need to install the developer dependencies with `npm` or `yarn` (preferably Yarn it's a big improvement! :smiley:), and run `yarn run build` (if you're on `npm` you can run `npm run build-npm` instead).

## App Structure

The application is contained in the `src/` directory, there are six main files to the application along with a set of tests for the utilities.

* `api.js` - This file has the interface to the Imgur API, it contains a single thunk function `fetchImages` that fetches the first five pages of Imgur's frontpage (minus gifs, albums and the NSFW content). It dispatches to our Redux-like dispatcher on success/failure to load in the images.
* `app.js` - The main entry point of the application, it does the work of building the application's initial state, houses the reducer function to handle the state and mounts and runs the application.
* `components.js` - This file houses the individual component functions to render our application. We have three components to the application, `ImageCard`, `Lightbox`, and `App`. `App` houses the layout of the application and utilizes `ImageCard` and `Lightbox` to produce our grid of images and the lightbox to view image details. The `Lightbox` component is a simple wrapper around the `ImageCard` along with some navigation buttons to move back/forward in the grid and to close the lightbox.
* `constants.js` - A simple file that houses some text constants used by our reducer and action creators.
* `rekt.js` - The React like library, it exports two interfaces, `createElement` and `render`. `createElement` takes a `tag`, `props` and `children` and produces a virtual DOM representation of HTML elements. The tag can have classes added to it in-line like `div.foo.bar`. `props` is an object of the properties on an element, event handlers (`onClick`, `onKeyDown` for example`), and other properties. `children` are the elements contained within the element you're creating. This argument can be a simple string for text content or an array of additional elements.
* `utils.js` - The utilities housed here are our `createStore` function that produces our Redux like state management system and the `fetch` function that wraps up a `GET` `XmlHttpRequest` into a Promise and sets our authorization header for Imgur.
* `tests.js` - Contains a set of tests for our `createElement` and `createStore` utilities to ensure they produce the expected results. These can be run by loading the `test.html` file in your local environment after building the application.